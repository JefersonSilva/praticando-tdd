insert into PESSOA (PESSOA_ID, NOME, TIPO) values (1, 'Goku', 'F');
insert into PESSOA (PESSOA_ID, NOME, TIPO) values (2, 'Buma', 'F');
insert into PESSOA (PESSOA_ID, NOME, TIPO) values (3, 'Bob Esponja', 'F');
insert into PESSOA (PESSOA_ID, NOME, TIPO) values (4, 'Sr Sirigueijo', 'F');

insert into PESSOA (PESSOA_ID, NOME, TIPO) values (5, 'Corporação Capsula', 'J');
insert into PESSOA (PESSOA_ID, NOME, TIPO) values (6, 'Siri Cascudo', 'J');
insert into PESSOA (PESSOA_ID, NOME, TIPO) values (7, 'Pizzaria do Pateta', 'J');

insert into PESSOA_FISICA (PESSOA_ID, CPF) values (1, '274.711.619-06');
insert into PESSOA_FISICA (PESSOA_ID, CPF) values (2, '186.535.478-35');
insert into PESSOA_FISICA (PESSOA_ID, CPF) values (3, '642.867.106-96');
insert into PESSOA_FISICA (PESSOA_ID, CPF) values (4, '274.711.619-06');

insert into PESSOA_JURIDICA (PESSOA_ID, CNPJ) values (5, '66.531.134/0001-47');
insert into PESSOA_JURIDICA (PESSOA_ID, CNPJ) values (6, '16.834.622/0001-55');
insert into PESSOA_JURIDICA (PESSOA_ID, CNPJ) values (7, '02.012.317/0001-73');

insert into CONTA (CONTA_ID, AGENCIA, DIGITO, NUMERO) values (1, '0001', '1', '11111');
insert into CONTA (CONTA_ID, AGENCIA, DIGITO, NUMERO) values (2, '0001', '1', '22222');
insert into CONTA (CONTA_ID, AGENCIA, DIGITO, NUMERO) values (3, '0001', '1', '33333');
insert into CONTA (CONTA_ID, AGENCIA, DIGITO, NUMERO) values (4, '0001', '1', '44444');


