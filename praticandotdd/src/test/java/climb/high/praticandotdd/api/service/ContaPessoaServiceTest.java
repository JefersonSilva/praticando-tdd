package climb.high.praticandotdd.api.service;

import static com.googlecode.catchexception.CatchException.catchException;
import static com.googlecode.catchexception.CatchException.caughtException;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.util.Arrays;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ArgumentsSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import climb.high.praticandotdd.api.service.impl.ContaPessoaServiceImpl;
import climb.high.praticandotdd.domain.dto.resource.ContaPessoaCadastroDto;
import climb.high.praticandotdd.domain.dto.resource.PessoaResultadoBuscaDto;
import climb.high.praticandotdd.domain.entity.Conta;
import climb.high.praticandotdd.domain.entity.ContaPessoa;
import climb.high.praticandotdd.domain.entity.PessoaFisica;
import climb.high.praticandotdd.domain.entity.PessoaJuridica;
import climb.high.praticandotdd.domain.exception.PessoaNotFoundException;
import climb.high.praticandotdd.domain.repository.ContaPessoaRepository;
import uk.co.jemos.podam.api.PodamFactory;
import uk.co.jemos.podam.api.PodamFactoryImpl;

@ExtendWith(MockitoExtension.class)
public class ContaPessoaServiceTest {

	private final PodamFactory podamFactory = new PodamFactoryImpl();

	@InjectMocks
	private ContaPessoaServiceImpl contaPessoaService;

	@Mock
	private PessoaService pessoaService;

	@Mock
	private ContaPessoaRepository contaPessoaRepository;

	@Test
	public void dadoInformacoesValidas_quandoAdicionarContaPessoa_eClienteNaoExistir_deveRetornarPessoaNotFound() {
		// given
		ContaPessoaCadastroDto contaPessoaCadastro = podamFactory.manufacturePojo(ContaPessoaCadastroDto.class);

		when(pessoaService.buscarPorTipoEDocumento(any(), any())).thenThrow(PessoaNotFoundException.class);

		// when
		catchException(contaPessoaService).adicionarContaPessoa(contaPessoaCadastro);

		// then
		assert caughtException() instanceof PessoaNotFoundException;
		verify(pessoaService, times(1)).buscarPorTipoEDocumento(any(), any());
		verifyNoMoreInteractions(pessoaService, contaPessoaRepository);
	}

	@Test
	public void dadoInformacoesValidas_quandoAdicionarContaPessoa_entaoContaCadastradaComSucesso() {
		// given
		ContaPessoaCadastroDto contaPessoaCadastro = podamFactory.manufacturePojo(ContaPessoaCadastroDto.class);
		contaPessoaCadastro.setDocumento("00000000019");
		contaPessoaCadastro.setAgencia("789");
		contaPessoaCadastro.setDigito("3");
		contaPessoaCadastro.setTipoPessoa("PF");

		PessoaResultadoBuscaDto pessoaResultadoBuscaDto = podamFactory.manufacturePojo(PessoaResultadoBuscaDto.class);
		pessoaResultadoBuscaDto.setTipoPessoa("PF");

		var novaContaPessoa = new ContaPessoa();
		novaContaPessoa.setConta(new Conta());
		novaContaPessoa.setPessoa(new PessoaFisica());
		novaContaPessoa.setTipoParticipacaoConta("Titular");

		when(pessoaService.buscarPorTipoEDocumento(any(), any())).thenReturn(pessoaResultadoBuscaDto);

		when(contaPessoaRepository.save(Mockito.any())).thenReturn(novaContaPessoa);

		// when
		contaPessoaService.adicionarContaPessoa(contaPessoaCadastro);

		// then
		verify(pessoaService, times(1)).buscarPorTipoEDocumento(any(), any());

		ArgumentCaptor<ContaPessoa> argument = ArgumentCaptor.forClass(ContaPessoa.class);
		verify(contaPessoaRepository, times(1)).save(argument.capture());

		ContaPessoa contaPessoaCapture = argument.getValue();
		assertEquals(contaPessoaCadastro.getAgencia(), contaPessoaCapture.getConta().getAgencia());
		assertEquals(contaPessoaCadastro.getDigito(), contaPessoaCapture.getConta().getDigito());
		assertEquals(contaPessoaCadastro.getTipoPessoa(), contaPessoaCapture.getPessoa().getTipo());
		assertEquals("Titular", contaPessoaCapture.getTipoParticipacaoConta());
		assertEquals(PessoaFisica.class, contaPessoaCapture.getPessoa().getClass());
	}

	@ParameterizedTest
	@ArgumentsSource(ContaPessoaArgumentsProvider.class)
	public void parameterized_dadoInformacoesValidas_quandoAdicionarContaPessoa_entaoContaCadastradaComSucesso(
			ContaPessoaTestParams testParams) {
		// given
		var tiposDeRelacionamento = new String[] { "Titular", "Dependente" };
		var tiposDePessoa = new Class[] { PessoaFisica.class, PessoaJuridica.class };

		ContaPessoaCadastroDto contaPessoaCadastro = testParams.getCadastroDto();

		PessoaResultadoBuscaDto pessoaResultadoBuscaDto = testParams.getPessoaResultadoBusca();

		var novaContaPessoa = testParams.getContaPessoaAdicionada();

		when(pessoaService.buscarPorTipoEDocumento(any(), any())).thenReturn(pessoaResultadoBuscaDto);

		when(contaPessoaRepository.save(Mockito.any())).thenReturn(novaContaPessoa);

		// when
		contaPessoaService.adicionarContaPessoa(contaPessoaCadastro);

		// then
		verify(pessoaService, times(1)).buscarPorTipoEDocumento(any(), any());

		ArgumentCaptor<ContaPessoa> argument = ArgumentCaptor.forClass(ContaPessoa.class);
		verify(contaPessoaRepository, times(1)).save(argument.capture());

		ContaPessoa contaPessoaCapture = argument.getValue();
		assertEquals(contaPessoaCadastro.getAgencia(), contaPessoaCapture.getConta().getAgencia());
		assertEquals(contaPessoaCadastro.getDigito(), contaPessoaCapture.getConta().getDigito());
		assertEquals(contaPessoaCadastro.getTipoPessoa(), contaPessoaCapture.getPessoa().getTipo());

		assert Arrays.stream(tiposDeRelacionamento)
				.anyMatch(tipoRel -> tipoRel.equals(contaPessoaCapture.getTipoParticipacaoConta()));

		assert Arrays.stream(tiposDePessoa)
				.anyMatch(tipoPessoa -> tipoPessoa.equals(contaPessoaCapture.getPessoa().getClass()));

	}

	@ParameterizedTest // Substitui o @Test
	@ValueSource(strings = { "Titular", "Dependente" }) // executa o teste para cada um dos valores declarados
	public void exemploValueSource(String tipo) {
		var tiposDeRelacionamento = new String[] { "Titular", "Dependente" };

		assert Arrays.stream(tiposDeRelacionamento).anyMatch(tipoRel -> tipoRel.equals(tipo));
	}

	@ParameterizedTest
	@ArgumentsSource(ContaPessoaArgumentsProvider.class)
	public void exemploArgumentSourceEArgumentProvider(ContaPessoaTestParams params) {
		var tiposDeRelacionamento = new String[] { "Titular", "Dependente" };
		var tiposDePessoa = new String[] { "PJ", "PF", };
		var tipoRelacionamento = params.getCadastroDto().getTipoDeRelacionamento();
		var tipoPessoa = params.getCadastroDto().getTipoPessoa();

		assert Arrays.stream(tiposDeRelacionamento).anyMatch(tr -> tr.equals(tipoRelacionamento));

		assert Arrays.stream(tiposDePessoa).anyMatch(tp -> tp.equals(tipoPessoa));
	}

}
