package climb.high.praticandotdd.api.service;

import java.util.stream.Stream;

import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsProvider;

import climb.high.praticandotdd.domain.dto.resource.ContaPessoaCadastroDto;
import climb.high.praticandotdd.domain.dto.resource.PessoaResultadoBuscaDto;
import climb.high.praticandotdd.domain.entity.Conta;
import climb.high.praticandotdd.domain.entity.ContaPessoa;
import climb.high.praticandotdd.domain.entity.PessoaFisica;
import climb.high.praticandotdd.domain.entity.PessoaJuridica;
import uk.co.jemos.podam.api.PodamFactory;
import uk.co.jemos.podam.api.PodamFactoryImpl;

public class ContaPessoaArgumentsProvider implements ArgumentsProvider {

    private final PodamFactory podamFactory = new PodamFactoryImpl();

    @Override
    public Stream<? extends Arguments> provideArguments(ExtensionContext context) {

        var paramsContaPessoaTitular = obterParams("Titular", "PJ", 6, 1);
        var paramsContaPessoaDependente = obterParams("Dependente", "PF", 1, 2);

        var contasPessoas = new ContaPessoaTestParams[] { paramsContaPessoaTitular, paramsContaPessoaDependente };

        return Stream.of(contasPessoas).map(Arguments::of);
    }

    private ContaPessoaTestParams obterParams(String tipoRelacionamento, String tipoPessoa, int pessoaId, int contaId) {
        var contaPessoaTitular = podamFactory.manufacturePojo(ContaPessoaCadastroDto.class);
        contaPessoaTitular.setTipoDeRelacionamento(tipoRelacionamento);
        contaPessoaTitular.setTipoPessoa(tipoPessoa);

        PessoaResultadoBuscaDto pessoaResultadoBuscaDto = podamFactory.manufacturePojo(PessoaResultadoBuscaDto.class);
        pessoaResultadoBuscaDto.setTipoPessoa(tipoPessoa);

        var contaPessoa = podamFactory.manufacturePojo(ContaPessoa.class);
        contaPessoa.setConta(new Conta());
        contaPessoa.getConta().setContaId(contaId);

        switch (tipoPessoa) {
            case "PJ":
                contaPessoa.setPessoa(new PessoaJuridica());
            default:
                contaPessoa.setPessoa(new PessoaFisica());

        }
        contaPessoa.getPessoa().setPessoaId(pessoaId);
        contaPessoa.setTipoParticipacaoConta(tipoRelacionamento);

        var params = new ContaPessoaTestParams();
        params.setCadastroDto(contaPessoaTitular);
        params.setContaPessoaAdicionada(contaPessoa);
        params.setPessoaResultadoBusca(pessoaResultadoBuscaDto);
        return params;
    }
}
