package climb.high.praticandotdd.domain.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ArgumentsSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import climb.high.praticandotdd.api.service.ContaPessoaArgumentsProvider;
import climb.high.praticandotdd.api.service.ContaPessoaTestParams;
import climb.high.praticandotdd.domain.entity.Conta;
import climb.high.praticandotdd.domain.entity.ContaPessoa;
import climb.high.praticandotdd.domain.entity.Pessoa;
import climb.high.praticandotdd.domain.entity.PessoaFisica;
import uk.co.jemos.podam.api.PodamFactory;
import uk.co.jemos.podam.api.PodamFactoryImpl;

@DataJpaTest(showSql = true)
public class ContaPessoaRepositoryTest {
	PodamFactory _podamFactory = new PodamFactoryImpl();

	@Autowired
	public ContaPessoaRepository _contaPessoaRepository;

	/**
	 * Teste de modelo
	 * 
	 * valida se o modelo (banco) da aplicação sobe sem erros
	 * 
	 * dados de domínio e pre requisitos declarados no test/resources/data.sql
	 */
	@Test
	public void validaModelo_adicionaNovaContaPessoa() {
		// given
		Conta conta = _podamFactory.manufacturePojo(Conta.class);
		conta.setContaId(1);
		Pessoa pessoa = _podamFactory.manufacturePojo(PessoaFisica.class);
		pessoa.setPessoaId(1);

		ContaPessoa contaPessoa = new ContaPessoa();
		contaPessoa.setConta(conta);
		contaPessoa.setPessoa(pessoa);
		contaPessoa.setTipoParticipacaoConta("Titular");

		// when
		ContaPessoa result = _contaPessoaRepository.save(contaPessoa);

		// then
		assertEquals(1, result.getConta().getContaId());
		assertEquals(1, result.getPessoa().getPessoaId());
	}

	/**
	 * Teste de modelo
	 * 
	 * valida se o modelo (banco) da aplicação sobe sem erros
	 * 
	 * dados de domínio e pre requisitos declarados no test/resources/data.sql
	 */
	@ParameterizedTest
	@ArgumentsSource(ContaPessoaArgumentsProvider.class)
	public void parameterized_validaModelo_adicionaNovaContaPessoa(ContaPessoaTestParams params) {
		// given
		ContaPessoa contaPessoa = params.getContaPessoaAdicionada();

		// when
		ContaPessoa result = _contaPessoaRepository.save(contaPessoa);

		// then
		assertNotNull(result.getConta().getNumero());
		assertNotNull(result.getPessoa().getNome());
	}
}
