package climb.high.praticandotdd.domain.exception;

public class MultiMessageException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String[] messages;

	public MultiMessageException(String[] messages) {
		super(joinMessages(messages));
	}

	private static String joinMessages(String[] messages) {
		String message = String.join(", ", messages);
		return message;
	}
	
	public String[] getMessages() {
		return messages;
	}
}
