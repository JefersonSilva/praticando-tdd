package climb.high.praticandotdd.domain.dto.resource;

import lombok.Data;

@Data
public class PessoaCadastroDto {
	private String tipoPessoa;
	private String documento;
	private String tipoParticipacaoConta; 
}
