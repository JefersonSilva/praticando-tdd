package climb.high.praticandotdd.domain.entity;

import java.io.Serializable;

import lombok.Data;

@Data
public class ContaPessoaId implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private int conta;
	private int pessoa;

}