package climb.high.praticandotdd.domain.entity;

import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@Entity
@PrimaryKeyJoinColumn(name = "pessoaId")
public class PessoaFisica extends Pessoa {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String cpf;

	@Override
	public void setDocumento(String cpf) {
		this.cpf = cpf;
	}
}
