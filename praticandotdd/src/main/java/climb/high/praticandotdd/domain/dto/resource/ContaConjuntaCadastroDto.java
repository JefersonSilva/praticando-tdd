package climb.high.praticandotdd.domain.dto.resource;

import lombok.Data;

@Data
public class ContaConjuntaCadastroDto {
	private String agencia;
	private String numero;
	private String digito;
	private PessoaCadastroDto[] pessoas;
}
