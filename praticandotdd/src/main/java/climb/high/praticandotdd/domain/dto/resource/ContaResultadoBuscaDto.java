package climb.high.praticandotdd.domain.dto.resource;

import lombok.Data;

@Data
public class ContaResultadoBuscaDto {
	private int contaId;
	private String agencia;
	private String numero;
	private String digito;
	private PessoaResultadoBuscaDto[] pessoas;
}
