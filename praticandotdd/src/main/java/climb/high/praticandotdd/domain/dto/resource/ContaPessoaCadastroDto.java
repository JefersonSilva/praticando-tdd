package climb.high.praticandotdd.domain.dto.resource;

import javax.validation.constraints.Size;

import lombok.Data;

@Data
public class ContaPessoaCadastroDto {

	@Size(min = 2, max = 2)
	private String tipoPessoa;
	private String documento;
	private String agencia;
	private String numero;
	private String digito;
	private String tipoDeRelacionamento;
	
	
}
