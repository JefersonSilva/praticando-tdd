package climb.high.praticandotdd.api.service.impl;

import org.springframework.stereotype.Service;

import climb.high.praticandotdd.api.service.ContaPessoaService;
import climb.high.praticandotdd.api.service.PessoaService;
import climb.high.praticandotdd.domain.dto.resource.ContaPessoaCadastroDto;
import climb.high.praticandotdd.domain.dto.resource.PessoaResultadoBuscaDto;
import climb.high.praticandotdd.domain.entity.Conta;
import climb.high.praticandotdd.domain.entity.ContaPessoa;
import climb.high.praticandotdd.domain.entity.Pessoa;
import climb.high.praticandotdd.domain.entity.PessoaFisica;
import climb.high.praticandotdd.domain.entity.PessoaJuridica;
import climb.high.praticandotdd.domain.repository.ContaPessoaRepository;

@Service
public class ContaPessoaServiceImpl implements ContaPessoaService {

	private final PessoaService pessoaService;
	private final ContaPessoaRepository contaPessoaRepository;

	public ContaPessoaServiceImpl(PessoaService pessoaService, ContaPessoaRepository contaPessoaRepository) {
		this.pessoaService = pessoaService;
		this.contaPessoaRepository = contaPessoaRepository;
	}

	@Override
	public void adicionarContaPessoa(ContaPessoaCadastroDto contaPessoaCadastro) {

		PessoaResultadoBuscaDto pessoaDto = pessoaService.buscarPorTipoEDocumento(contaPessoaCadastro.getTipoPessoa(),
				contaPessoaCadastro.getDocumento());

		Conta conta = new Conta();
		conta.setAgencia(contaPessoaCadastro.getAgencia());
		conta.setDigito(contaPessoaCadastro.getDigito());
		conta.setNumero(contaPessoaCadastro.getNumero());

		Pessoa pessoa = criarNovaPessoaPorTipo(pessoaDto.getTipoPessoa());
		pessoa.setNome(pessoaDto.getNome());
		pessoa.setTipo(pessoaDto.getTipoPessoa());
		pessoa.setDocumento(pessoaDto.getDocumento());

		
		ContaPessoa contaPessoa = new ContaPessoa();
		contaPessoa.setConta(conta);
		contaPessoa.setPessoa(pessoa);
		// xau seus lindos <3
		contaPessoaRepository.save(contaPessoa);
	}

	private Pessoa criarNovaPessoaPorTipo(String tipoPessoa) {
		switch (tipoPessoa) {
			case "PJ":
				return new PessoaJuridica();
			default:
				return new PessoaFisica();
		}
	}
}
