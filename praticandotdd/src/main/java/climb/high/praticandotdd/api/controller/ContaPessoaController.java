package climb.high.praticandotdd.api.controller;

import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import climb.high.praticandotdd.api.service.ContaPessoaService;
import climb.high.praticandotdd.domain.dto.resource.ContaPessoaCadastroDto;
import climb.high.praticandotdd.domain.exception.RequiredFieldsException;

@RestController
@RequestMapping("/api/contapessoa")
public class ContaPessoaController {
	private ContaPessoaService contaPessoaService;

	/**
	 * Colinha:
	 * 
	 * GetMapping(value = "uri", produces = MediaType) PostMapping(consumes =
	 * MediaType, produces =MediaType) Valid RequestBody BindingResults PathVariable
	 */
	public ContaPessoaController(ContaPessoaService contaPessoaService) {
		this.contaPessoaService = contaPessoaService;
	}

	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(code = HttpStatus.CREATED)
	public void criarContaPessoa(@Valid @RequestBody ContaPessoaCadastroDto contaPessoaCadastroDto, BindingResult bindingResults) {
		
		if(bindingResults.hasErrors()) {
			throw new RequiredFieldsException();
		}
		
		
		contaPessoaService.adicionarContaPessoa(contaPessoaCadastroDto);
	}
}
