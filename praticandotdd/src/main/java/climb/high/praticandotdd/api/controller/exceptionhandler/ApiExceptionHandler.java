package climb.high.praticandotdd.api.controller.exceptionhandler;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import climb.high.praticandotdd.domain.exception.BusinessException;
import climb.high.praticandotdd.domain.exception.MultiMessageException;
import climb.high.praticandotdd.domain.exception.NotFoundException;
import climb.high.praticandotdd.domain.exception.RequiredFieldsException;
import climb.high.praticandotdd.domain.exception.ResponseException;

@ControllerAdvice
@RestController
public class ApiExceptionHandler extends ResponseEntityExceptionHandler {
	@ExceptionHandler(Exception.class)
	public final ResponseEntity<ResponseException> responderInternalServerError(Exception ex, WebRequest request) {
		var responseException = new ResponseException(ex.getMessage(), request.getDescription(false));
		return new ResponseEntity<>(responseException, HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	@ExceptionHandler(NotFoundException.class)
	public final ResponseEntity<ResponseException> responderNotFound(Exception ex, WebRequest request) {
		var responseException = new ResponseException(ex.getMessage(), request.getDescription(false));
		return new ResponseEntity<>(responseException, HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	@ExceptionHandler(RequiredFieldsException.class)
	public final ResponseEntity<ResponseException> responderUnprocessableEntity(MultiMessageException ex, WebRequest request) {
		var responseException = new ResponseException(ex.getMessages(), request.getDescription(false));
		return new ResponseEntity<>(responseException, HttpStatus.UNPROCESSABLE_ENTITY);
	}
	
	@ExceptionHandler(BusinessException.class)
	public final ResponseEntity<ResponseException> responderUnprocessableEntity(RuntimeException ex, WebRequest request) {
		var responseException = new ResponseException(ex.getMessage(), request.getDescription(false));
		return new ResponseEntity<>(responseException, HttpStatus.BAD_REQUEST);
	}
}
