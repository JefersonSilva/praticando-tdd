package climb.high.praticandotdd.api.service;

import climb.high.praticandotdd.domain.dto.resource.PessoaResultadoBuscaDto;

public interface PessoaService {
	PessoaResultadoBuscaDto buscarPorTipoEDocumento(String tipo, String documento); 
}
