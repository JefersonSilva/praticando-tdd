package climb.high.praticandotdd.api.service;

import climb.high.praticandotdd.domain.dto.resource.ContaPessoaCadastroDto;

public interface ContaPessoaService {

	void adicionarContaPessoa(ContaPessoaCadastroDto contaPessoaCadastro);

}
